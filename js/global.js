//SOUNDS
var audio = [], 
	audioPiece = [],
	sprite = [],
	typingText = [];

//TIMEOUTS
var launch = [], 
	globalName,
	tincan,
	timer;

//AWARDs NUMBER
var awardNum = 0;

//OXYGEN AMOUNT 
var oxygenAmount = 0,
	oxygenPointer;

var startTimer = function(jqueryElement, secondsLeft, callBack)
{
	if(secondsLeft < 0)
		callBack();
	else
	{
		if(secondsLeft > 9)
			jqueryElement.html("00:"+secondsLeft--);
		else
			jqueryElement.html("00:0"+secondsLeft--);
		timer = setTimeout(function(){
			startTimer(jqueryElement, secondsLeft, callBack);
		}, 1000);
	}
}

var stopTimer = function()
{
	clearTimeout(timer);
}
	
var drawPointer = function()
{
	switch(oxygenAmount)
	{
		case 0: oxygenPointer.css("transform", "rotate(-65deg)");
				break;
		case 10: oxygenPointer.css("transform", "rotate(-65deg)");
				break;
		case 20: oxygenPointer.css("transform", "rotate(-50deg)");
				break;
		case 30: oxygenPointer.css("transform", "rotate(-35deg)");
				break;
		case 40: oxygenPointer.css("transform", "rotate(-20deg)");
				break;
		case 50: oxygenPointer.css("transform", "rotate(-5deg)");
				break;
		case 60: oxygenPointer.css("transform", "rotate(10deg)");
				break;
		case 70: oxygenPointer.css("transform", "rotate(25deg)");
				break;
		case 80: oxygenPointer.css("transform", "rotate(40deg)");
				break;
		case 90: oxygenPointer.css("transform", "rotate(55deg)");
				break;
		case 100: oxygenPointer.css("transform", "rotate(70deg)");
				break;
	}
}

var allHaveHtml = function(jqueryElement){
	for(var i = 0; i < jqueryElement.length; i ++)
	{
		if(!$(jqueryElement[i]).html())
			return false;
	}
	return true;
}

var fadeOneByOne = function(jqueryElement, curr, interval, endFunction)
{
	if (curr < jqueryElement.length)
	{
		timeout[curr] = setTimeout(function(){
			$(jqueryElement[curr]).fadeIn(200);
			fadeOneByOne(jqueryElement, ++curr, interval, endFunction);
		}, interval);
	}
	else
		endFunction();
}

function TypingText(jqueryElement, interval, hasSound, endFunction)
{
	this.text = jqueryElement.html();
	this.jqueryElement = jqueryElement;
	this.interval = interval;
	this.currentSymbol = 0;
	this.endFunction = endFunction;
	this.hasSound = hasSound;
	this.audio = new Audio("audio/keyboard-sound.mp3");
	this.audio.addEventListener("ended", function(){
			this.play();
		});
	
	this.timeout;
	this.jqueryElement.html("");
}

TypingText.prototype.write = function()
{
	tObj = this;
	if (tObj.audio.paused && tObj.hasSound) {
		tObj.audio.play();
	}
	tObj.timeout = setTimeout(function(){
		if(tObj.text[tObj.currentSymbol] === "<")
		{
			var numToPass = tObj.currentSymbol + 1, 
				openTag = "<",
				text = "", 
				closeTag = "";
				
			while(tObj.text[numToPass] !== ">")
			{
				openTag += tObj.text[numToPass];
				numToPass ++;
			}
			
			openTag += ">";
			
			numToPass ++;
			
			if(openTag === "<br>")
				tObj.jqueryElement.append(openTag);
			else
			{
				while(tObj.text[numToPass] !== "<")
				{
					text += tObj.text[numToPass];
					numToPass ++;
				}
				
				while(tObj.text[numToPass] !== ">")
				{
					closeTag += tObj.text[numToPass];
					numToPass ++;
				}
				
				closeTag += ">";
				numToPass ++;

				tObj.jqueryElement.append(openTag + text + closeTag);
			}
			
			tObj.currentSymbol = numToPass;
		}
		else
		{
			tObj.jqueryElement.append(tObj.text[tObj.currentSymbol]);
			tObj.currentSymbol ++;
		}
		if (tObj.currentSymbol < tObj.text.length) 
		{
			tObj.write();
		}
		else
		{
			tObj.audio.pause();
			tObj.endFunction();
		}
	}, tObj.interval);
}

TypingText.prototype.stopWriting = function()
{
	clearTimeout(this.timeout);
	this.audio.pause();
	this.jqueryElement.html(this.text);
}

var loadImages = function(){
	jQuery.get('fileNames.txt', function(data) {
		var imagesSrc = data.split("\n"),
			images = [],
			loadPercentage = 0,
			imagesNum = imagesSrc.length - 1;
			unitToAdd = Math.round(100 / imagesNum);
		
		var imageLoadListener = function(){
			loadPercentage += unitToAdd;
			console.log(loadPercentage);
			imagesNum --;
			if (loadPercentage >= 100) {
				//hideEverythingBut($("#frame-000"));
			}
		};
		
		for (var i = 0; i < imagesSrc.length - 1; i ++)
		{
			images[i] = new Image();
			images[i].src = "pics/" + imagesSrc[i];
			images[i].addEventListener("load", imageLoadListener);
		}
	});
}

function DragTask(jqueryElements, successCondition, successFunction, failFunction, finishCondition, finishFunction)
{
	this.draggables = jqueryElements;
	this.draggabillies = [];
	this.vegetable;
	this.basket;
	
	this.makeThemDraggable = function()
	{
		for(var i = 0; i < this.draggables.length; i++)
			this.draggabillies[i] = new Draggabilly(this.draggables[i]);
	}
	
	this.addEventListeners = function()
	{
		for(var i = 0; i < this.draggabillies.length; i++)
		{
			this.draggabillies[i].on("dragStart", this.onStart);
			this.draggabillies[i].on("dragEnd", this.onEnd);
		}
	}
	
	this.onEnd = function(instance, event, pointer)
	{
		var currVeg = this.vegetable;
		var currBasket = this.basket;
		currVeg.fadeOut(0);
		currBasket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		currVeg.fadeIn(0);
		
		if (currBasket.attr("data-key") && successCondition(currVeg.attr("data-key"), currBasket.attr("data-key")))
				successFunction(currVeg, currBasket);
		else
			failFunction(currVeg, currBasket);
		
		currVeg.removeClass("box-shadow-white");
		currVeg.css("opacity", "");
		
		if (finishCondition())
		{
			finishFunction();
		}
	}
	
	this.onStart = function(instance, event, pointer)
	{
		this.vegetable = $(event.target);
		this.vegetable.css("z-index", "9999");
		this.vegetable.addClass("box-shadow-white");
		this.vegetable.css("opacity", "0.6");
	}
	
	this.makeThemDraggable();
	this.addEventListeners();
}

var blink = function(jqueryElements, interval, times)
{
	var intervalHalf = Math.round(interval/2);
	timeout[0] = setTimeout(function(){
		jqueryElements.css("outline", "3px solid red");
		timeout[1] = setTimeout(function(){
			jqueryElements.css("outline", "");
			if (times) 
				blink(jqueryElements, interval, --times)		
		}, intervalHalf);
	}, intervalHalf);
}

var setLRSData = function(){
	tincan = new TinCan (
    {
        recordStores: [
            {
                "endpoint": "http://54.154.57.220/data/xAPI/",
				"username": "e083498f4e256e68ab2c5ae2be4195d9a348eb20",
				"password": "c6ea3c32a9d77919d0eb3cdea3bc5d460f50d93b"
            }
        ]
    });
}

var sendLaunchedStatement = function(sm)
{
	if(globalName)
	{
		tincan.sendStatement(
		{
			"actor": {
				"objectType": "Agent",
				"account": {
					"name": globalName,
					"homePage": "http://lcms.nis.kz"
				}
			},
			"verb": {
				"id": "http://adlnet.gov/expapi/verbs/launched"
			},
			"context": {
				"platform": "web"
			},
			"object": {
				"id": "http://lcms.nis.kz/activity/821eb006-58bb-4154-b732-4b2a9a9330ad",
				"objectType": "Activity",
				"description": "frame-" + sm
			}
		});
	}
}

var sendCompletedStatement = function(sm)
{
	if(globalName)
	{
		tincan.sendStatement(
		{
			"actor": {
				"objectType": "Agent",
				"account": {
					"name": globalName,
					"homePage": "http://lcms.nis.kz/chemistry"
				}
			},
			"verb": {
				"id": "http://adlnet.gov/expapi/verbs/completed"
			},
			"context": {
				"platform": "web"
			},
			"object": {
				"id": "http://lcms.nis.kz/activity/821eb006-58bb-4154-b732-4b2a9a9330ad",
				"objectType": "Activity",
				"description": "frame-" + sm
			}
		});
	}
}

setMenuStuff = function()
{
	var enterButton = $(".enter-button"),
		annotation = $("#frame-000 .annotation-hidden"),
		annotationButton = $("#frame-000 .annotation-button"),
		password = $(".password"),
		error = $(".error"),
		name = $(".name");
		name.val("");
		password.val("");
		
		
	sayHello = function(){
		regBox.html("Здравствуйте, " + name.val() + "!");
		regBox.css("height", "3em");
		regBox.css("padding", "0.5em");
		regBox.css("text-align", "center");
		globalName = name.val();
		regBox.addClass("topcorner");
		timeout[0] = setTimeout(function(){
			regBox.html(name.val());
			regBox.css("width", name.val().length + "em");
		}, 3000);
	};
	
	if(globalName)
		sayHello();
	
	var annotationButtonListener = function(){
		annotation.toggleClass("annotation-shown");
		annotationButton.toggleClass("annotation-button-close");
	};
	annotationButton.off("click", annotationButtonListener);
	annotationButton.on("click", annotationButtonListener);
	
	var enterButtonListener = function(){
		if(password.val() === "12345")
			sayHello();
		else
			error.html("неверный пароль");
	};
	enterButton.off("click", enterButtonListener);
	enterButton.on("click", enterButtonListener);
}

launch["frame-000"] = function()
{
}

launch["frame-101"] = function()
{
		theFrame = $("#frame-101"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		text = $(prefix + ".explanation-container"), 
		o2 = $(".o2");
	
	o2.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
		
	typingText[0] = new TypingText(text, 100, true, function(){
		timeout[7] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			hideEverythingBut($("#frame-102"));
			sendCompletedStatement(1);
		}, 2000);
	});
		
	startButtonListener = function(){
		timeout[1] = setTimeout(function(){
			typingText[0].write();
		}, 1000);
		timeout[2] = setTimeout(function(){
			o2.fadeIn(0);
		}, 7000);
		timeout[3] = setTimeout(function(){
			o2.css({
				"width": "40%",
				"height": "46%",
				"left": "30%",
				"top": "25%"
			});
		}, 8000);
		timeout[4] = setTimeout(function(){
			oxygenAmount += 100;
			drawPointer();
		}, 10000);
		timeout[5] = setTimeout(function(){
			oxygenAmount -= 100;
			drawPointer();
		}, 13000);
		timeout[6] = setTimeout(function(){
			o2.css({
				"width": "",
				"height": "",
				"left": "",
				"top": ""
			});
		}, 15000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(1);
	}, 2000);
}

launch["frame-102"] = function()
{
		theFrame = $("#frame-102"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		ship = $(prefix + ".ship"),
		blackSkin = $(prefix + ".black-skin");
	
	audio[0] = new Audio("audio/spacecraft-sound.mp3");

	sprite[0] = new Motio(ship[0], {
		"fps": "1.5", 
		"frames": "5"
	});
	
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
		}	
	});
	
	oxygenAmount = 100;
	drawPointer();
	
	ship.fadeOut(0);
	sprite[0].to(0, true);
	fadeNavsOut();
	fadeLauncherIn();
	blackSkin.css("opacity", "");
	
	startButtonListener = function(){
		timeout[1] = setTimeout(function(){
			audio[0].play();
			ship.fadeIn(500);
			blackSkin.addClass("transition-5s");
			timeout[2] = setTimeout(function(){
				sprite[0].play();
			}, 1000);
			timeout[3] = setTimeout(function(){
				blackSkin.css("opacity", "1");
			}, 2000);
			timeout[4] = setTimeout(function(){
				sendCompletedStatement(2);
				theFrame.attr("data-done", "true");
				hideEverythingBut($("#frame-103"));
			}, 5000);
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(2);
	}, 2000);
}

launch["frame-103"] = function()
{
		theFrame = $("#frame-103"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		words = $(prefix + ".word"), 
		atomStructure = $(prefix + ".atom-structure-rus, "+prefix + ".atom-structure-kaz, " + prefix + ".atom-structure-eng"),
		protonNumber = $(prefix + ".proton-number-rus, " + prefix + ".proton-number-kaz, " + prefix + ".proton-number-eng"),
		isotope = $(prefix + ".isotope-rus, " + prefix + ".isotope-kaz, " + prefix + ".isotope-eng"),
		massNumber = $(prefix + ".mass-number-rus, " + prefix + ".mass-number-kaz, " + prefix + ".mass-number-eng"),
		nuclei = $(prefix + ".nuclei-rus, " + prefix + ".nuclei-kaz, " + prefix + ".nuclei-eng");
	
	audio[0] = new Audio("audio/sci-fi.mp3");
	
	audio[0].addEventListener("ended", function(){
		this.play();
	});
	
	oxygenAmount -= 10;
	drawPointer();
	words.css("left", "2000px");
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
		audio[0].play();
		words.addClass("transition-5s");
		atomStructure.css("left", "");
		timeout[1] = setTimeout(function(){
			protonNumber.css("left", "");
		}, 5000);
		timeout[2] = setTimeout(function(){
			isotope.css("left", "");
		}, 7000);
		timeout[3] = setTimeout(function(){
			massNumber.css("left", "");
		}, 9000);
		timeout[4] = setTimeout(function(){
			nuclei.css("left", "");
		}, 11000);
		timeout[5] = setTimeout(function(){
			audio[0].pause();
			sendCompletedStatement(3);
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
		}, 15000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(3);
	}, 2000);
}

launch["frame-104"] = function()
{
		theFrame = $("#frame-104"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		labels = $(prefix + ".labels"),
		items = $(prefix + ".item");
	
	audio[0] = new Audio("audio/s4-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		timeout[4] = setTimeout(function(){
			sendCompletedStatement(4);
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
		}, 3000);
	});
	
	labels.fadeOut(0);
	items.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	oxygenAmount -= 10;
	drawPointer();
			
	startButtonListener = function(){
		timeout[1] = setTimeout(function(){
			audio[0].play();
			timeout[2] = setTimeout(function(){
				items.fadeIn(1000);
				timeout[3] = setTimeout(function(){
					labels.fadeIn(1000);
				}, 1000);
			}, 3000);
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(4);
	}, 2000);
}

launch["frame-105"] = function()
{
		theFrame = $("#frame-105"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		label = $(prefix + ".label"),
		labelImage = $(prefix + ".label-image"),
		portrait = $(prefix + ".portrait");
	
	audio[0] = new Audio("audio/s5-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		items.fadeIn(1000);
		audio[1].play();
		timeout[3] = setTimeout(function(){
			labels.fadeIn(1000);
		}, 1000);
	});
	
	typingText[0] = new TypingText(label, 50, true, function(){
		timeout[4] = setTimeout(function(){
			sendCompletedStatement(5);	
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
		}, 3000);
	});
	
	label.fadeOut(0);
	portrait.fadeOut(0);
	labelImage.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	oxygenAmount -= 10;
	drawPointer();
			
	startButtonListener = function(){
		timeout[1] = setTimeout(function(){
			audio[0].play();
			label.fadeIn(500);
			labelImage.fadeIn(1000);
			typingText[0].write();
			timeout[2] = setTimeout(function(){
				portrait.fadeIn(500);
			}, 2000);
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(5);
	}, 2000);
}

launch["frame-106"] = function()
{
		theFrame = $("#frame-106"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		cloud = $(prefix + ".s6-stroenie"),
		electrons = $(prefix + ".electrons"),
		label = $(prefix + ".label");
	
	sprite[0] = new Motio(electrons[0], {
		"fps": "2", 
		"frames": "3"
	});
	
	audio[0] = new Audio("audio/s6-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		timeout[5] = setTimeout(function(){
			sendCompletedStatement(6);
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
		}, 2000);
	});
	
	cloud.fadeOut(0);
	label.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	oxygenAmount -= 10;
	drawPointer();
			
	startButtonListener = function(){
		timeout[1] = setTimeout(function(){
			audio[0].play();
		}, 2000);
		timeout[2] = setTimeout(function(){
			label.fadeIn(1000);
		}, 4000);
		timeout[3] = setTimeout(function(){
			cloud.fadeIn(1000);
		}, 8000);
		timeout[4] = setTimeout(function(){
			sprite[0].play();
		}, 18000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(6);
	}, 2000);
}

launch["frame-107"] = function()
{
		theFrame = $("#frame-107"),
		theClone = theFrame.clone();
	var prefix = "#" + theFrame.attr("id") + " ",
		items = $(prefix + ".item");
	
	audio[0] = new Audio("audio/s7-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		timeout[2] = setTimeout(function(){
			sendCompletedStatement(7);
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
		}, 3000);
	});
	
	items.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	oxygenAmount -= 10;
	drawPointer();
			
	startButtonListener = function(){
		timeout[1] = setTimeout(function(){
			audio[0].play();
			fadeOneByOne(items, 0, 1000, function(){});
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(7);
	}, 2000);
}

launch["frame-108"] = function()
{
		theFrame = $("#frame-108"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		area = $(prefix + ".area"),
		nucleonNumber = $(prefix + ".nucleon-number"),
		nucleonNumberLabel = $(prefix + ".nucleon-number .label"),
		k = $(prefix + ".K");
	
		typingText[0] = new TypingText(area, 80, false, function(){
		
	}); 
	
	sprite[0] = new Motio(nucleonNumber[0], {
		"fps": "1",
		"frames": "4"
	});
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
		}
	});
	
	sprite[0].to(3, true);
	
	audio[0] = new Audio("audio/s8-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		timeout[3] = setTimeout(function(){
			sendLaunchedStatement(8);
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
		}, 3000);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	nucleonNumberLabel.css("color", "transparent");
	nucleonNumber.fadeOut(0);
	area.fadeOut(0);	
	
	oxygenAmount -= 10;
	drawPointer();
			
	startButtonListener = function(){
		timeout[1] = setTimeout(function(){
			area.fadeIn(500);
			typingText[0].write();		
			audio[0].play();
		}, 1000);
		timeout[2] = setTimeout(function(){
			area.fadeOut(0);
			k.fadeOut(0);
			nucleonNumber.fadeIn(1000);
			sprite[0].play();
			nucleonNumberLabel.css("color", "white");
		}, 28000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(8);
	}, 2000);
}

launch["frame-109"] = function()
{
		theFrame = $("#frame-109"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		task = $(prefix + ".task"),
		text = $(prefix + ".text"),
		balls = $(prefix + ".ball"),
		periodicTable = $(prefix + ".periodic-table"),
		helpButton = $(prefix + ".help-button"),
		keepThinking = $(prefix + ".keep-thinking"),
		hint = $(prefix + ".hint"), 
		taskTimer = $(prefix + ".task-timer");

	typingText[0] = new TypingText(task, 50, true, function(){
		timeout[7] = setTimeout(function(){
			balls.fadeIn(500);
			text.fadeIn(500);
			task.fadeOut(0);
			startTimer(taskTimer, 60, function(){
				hint.fadeIn(1000);
				timeout[8] = setTimeout(function(){
					theFrame.attr("data-done", "true");
					fadeNavsInAuto();
				}, 3000);				
			});
		}, 1000);
	});
	
	var successCondition = function(vegetable, basket)
	{
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket)
	{
		basket.html(vegetable.html());
		basket.css("color", vegetable.css("color"));
		basket.css("font-size", vegetable.css("font-size"));
		basket.css("font-style", vegetable.css("font-style"));
		vegetable.remove();
		oxygenAmount += 10;
		drawPointer();
	};
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
		keepThinking.fadeIn(0);
		timeout[6] = setTimeout(function(){
			keepThinking.fadeOut(0);
		}, 3000);
	};
	
	var finishCondition = function(vegetable, basket)
	{
		return $(prefix + ".ball").length <= 10;
	};
	
	var finishFunction = function(vegetable, basket)
	{
		hint.fadeIn(1000);
		stopTimer();
		audio[3].play();
		timeout[4] = setTimeout(function(){
			audio[4].play();
		}, 7000);
		timeout[5] = setTimeout(function(){
			sendCompletedStatement(9);
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
		}, 22000);
	};
	
	var helpListener = function(){
		if(parseInt(periodicTable.css("height")))
			periodicTable.css("height", "0%");
		else
			periodicTable.css("height", "100%");
	};
	
	helpButton.off("click", helpListener);
	helpButton.on("click", helpListener);
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	audio[0] = new Audio("audio/alarm-sound.mp3");
	audio[1] = new Audio("audio/sci-fi.mp3");
	audio[2] = new Audio("audio/s9-1.mp3");
	audio[3] = new Audio("audio/s11-1.mp3");
	audio[4] = new Audio("audio/s12-1.mp3");
	
	audio[1].addEventListener("ended", function(){
		audio[1].play();
	});	
	audio[2].addEventListener("ended", function(){
		timeout[3] = setTimeout(function(){
			typingText[0].write();
			audio[1].play();
		}, 1000);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	balls.fadeOut(0);
	text.fadeOut(0);
	hint.fadeOut(0);
	periodicTable.css("height", "0%");
	periodicTable.css("opacity", "0.9");
	keepThinking.fadeOut(0);
	
	startButtonListener = function(){
		timeout[1] = setTimeout(function(){
			periodicTable.addClass("transition-2s");
			oxygenAmount -= 10;
			drawPointer();
			audio[0].volume = 0.2;
			audio[1].volume = 0.2;
			audio[0].play();
			timeout[2] = setTimeout(function(){
				audio[0].pause();
				audio[2].play();
			}, 3000);			
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(9);
	}, 2000);
}

launch["frame-110"] = function()
{
		theFrame = $("#frame-110"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		text = $(prefix + ".text"),
		blank = $(prefix + ".blank"),
		task = $(prefix + ".task"),
		balls = $(prefix + ".ball"), 
		checkButton = $(prefix + ".check-button"), 
		helpButton = $(prefix + ".help-button"),
		periodicTable = $(prefix + ".periodic-table"),
		answer = $(prefix + ".answer"),
		b1proton = 0,
		b1neutron = 0,
		b2proton = 0,
		b2neutron = 0, 
		b3proton = 0, 
		b3neutron = 0;
	
	typingText[0] = new TypingText(text, 50, true, function(){
		timeout[2] = setTimeout(function(){
			audio[0].play();
			task.fadeIn(500);
			blank.fadeIn(500);
			helpButton.fadeIn(500);
			balls.fadeIn(500);
		}, 2000);
	});
	
	audio[0] = new Audio("audio/s13-1.mp3");
	audio[1] = new Audio("audio/s15-1.mp3");
	
	audio[1].addEventListener("ended", function(){
		sendCompletedStatement(10);
		theFrame.attr("data-done", "true");
		fadeNavsInAuto();
	});
	
	//DragTask(jqueryElements, successCondition, successFunction, failFunction, finishCondition, finishFunction)
	var successCondition = function(vegetable, basket)
	{
		return basket === "basket";
	}
	var successFunction = function(vegetable, basket)
	{
		
		var temp = vegetable.clone();
		temp.removeClass("box-shadow-white");
		temp.css("z-index", "2");
		$(prefix).append(temp);
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
		
		if(basket.hasClass("basket-1"))
		{
			if(vegetable.hasClass("proton"))
				b1proton ++;
			else
				b1neutron ++;
		}
		
		if(basket.hasClass("basket-2"))
		{
			if(vegetable.hasClass("proton"))
				b2proton ++;
			else
				b2neutron ++;
		}
		
		if(basket.hasClass("basket-3"))
		{
			if(vegetable.hasClass("proton"))
				b3proton ++;
			else
				b3neutron ++;
		}
		
		console.log("b1proton: " + b1proton);
		console.log("b1neutron: " + b1neutron);
		console.log("b2proton: " + b2proton);
		console.log("b2neutron: " + b2neutron);
		console.log("b3proton: " + b3proton);
		console.log("b3neutron: " + b3neutron);
		
		if((b1proton || b1neutron) &&
			(b2proton || b2neutron) &&
			(b3proton || b3neutron))
			checkButton.fadeIn(500);
	}
	var failFunction = function(vegetable, basket)
	{
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
	}
	var finishCondition = function()
	{
	}
	var finishFunction = function()
	{
	}
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	fadeNavsOut();
	fadeLauncherIn();
	answer.fadeOut(0);
	blank.fadeOut(0);
	balls.fadeOut(0);
	task.fadeOut(0);
	helpButton.fadeOut(0);
	text.fadeOut(0);
	checkButton.fadeOut(0);
	periodicTable.css("height", "0%");
	
	var helpListener = function(){
		if(parseInt(periodicTable.css("height")))
			periodicTable.css("height", "0%");
		else
			periodicTable.css("height", "100%");
	};
	helpButton.off("click", helpListener);
	helpButton.on("click", helpListener);
	
	var checkButtonListener = function(){
		answer.fadeIn(0);
		if(b1proton === 4)
			oxygenAmount += 5;
		if(b1neutron === 5)
			oxygenAmount += 5;
		if(b2proton === 8)
			oxygenAmount += 5;
		if(b2neutron === 8)
			oxygenAmount += 5;
		if(b2proton === 12)
			oxygenAmount += 5;
		if(b2neutron === 12)
			oxygenAmount += 5;
		
		drawPointer();
		audio[1].play();
	}
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	startButtonListener = function(){
		timeout[1] = setTimeout(function(){
			text.fadeIn(500);
			periodicTable.addClass("transition-2s");
			typingText[0].write();
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(10);
	}, 2000);
}

launch["frame-111"] = function()
{
		theFrame = $("#frame-111"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		text = $(prefix + ".text"),
		task = $(prefix + ".task"),
		table = $(prefix + ".table"),
		inputs = $(prefix + "input"),
		checkButton = $(prefix + ".check-button"),
		helpButton = $(prefix + ".help-button"),
		periodicTable = $(prefix + ".periodic-table");
	
	audio[0] = new Audio("audio/sci-fi.mp3");
	audio[1] = new Audio("audio/s17-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		this.play();
	});
	
	audio[1].addEventListener("ended", function(){
		timeout[4] = setTimeout(function(){
			task.fadeIn(500);
			typingText[0].write();
		}, 2000);
	});
	
	typingText[0] = new TypingText(task, 50, true, function(){
		table.fadeIn(500);
		helpButton.fadeIn(0);
	});
	
	inputs.val("");
	
	fadeNavsOut();
	fadeLauncherIn();
	oxygenAmount -= 10;
	drawPointer();
	checkButton.fadeOut(0);
	text.fadeOut(0);
	table.fadeOut(0);
	task.fadeOut(0);
	periodicTable.css("height", "0%");
	helpButton.fadeOut(0);
	
	var helpListener = function(){
		if(parseInt(periodicTable.css("height")))
			periodicTable.css("height", "0%");
		else
			periodicTable.css("height", "100%");
	};
	helpButton.off("click", helpListener);
	helpButton.on("click", helpListener);
	
	var inputListener = function(){
		var emptyNum = 14;
		for(var i = 0; i < inputs.length; i++)
			if($(inputs[i]).val())
				emptyNum --;
			
		if(!emptyNum)
			checkButton.fadeIn(500);
	};
	inputs.on("keyup", inputListener); //jquery 
	
	var checkButtonListener = function(){
		for(var i = 0; i < inputs.length; i++)
		{
			if($(inputs[i]).val() === $(inputs[i]).attr("data-correct"))
			{
				oxygenAmount += 5;
				$(inputs[i]).css("color", "green");
			}
			else
			{
				$(inputs[i]).css("color", "red");
			}
			drawPointer();
		}
		
		timeout[2] = setTimeout(function(){
			checkButton.fadeOut(0);
			for(var i = 0; i < inputs.length; i++)
			{
				if($(inputs[i]).val() === $(inputs[i]).attr("data-correct"))
				{
					$(inputs[i]).css("color", "");
					$(inputs[i]).css("font-weight", "bold");
					$(inputs[i]).css("font-size", "1.3vmax");
				}
				else
				{
					$(inputs[i]).css("color", "");
					$(inputs[i]).css("color", "normal");
					$(inputs[i]).val($(inputs[i]).attr("data-correct"));
					$(inputs[i]).css("font-size", "1.1vmax");
				}
			}
		}, 3000);
		
		timeout[3] = setTimeout(function(){
			sendCompletedStatement(11);
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
		}, 8000);
		
		drawPointer();
	}
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	startButtonListener = function(){
		timeout[1] = setTimeout(function(){
			periodicTable.addClass("transition-2s");
			text.fadeIn(1000);
			audio[0].volume = 0.3;
			audio[0].play();
			audio[1].play();	
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(11);
	}, 2000);
}

launch["frame-112"] = function()
{
		theFrame = $("#frame-112"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		helpButton = $(prefix + ".help-button"),
		periodicTable = $(prefix + ".periodic-table"),
		balls = $(prefix + ".ball"),
		keepThinking = $(prefix + ".keep-thinking"),
		task = $(prefix + ".task"),
		textContainer = $(prefix + ".text-container"), 
		helpButton = $(prefix + ".help-button"),
		periodicTable = $(prefix + ".periodic-table");
	
	audio[0] = new Audio("audio/s21-1.mp3");
	audio[1] = new Audio("audio/s23-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		typingText[0].write();
		task.fadeIn(500);
	});
	
	var helpListener = function(){
		if(parseInt(periodicTable.css("height")))
			periodicTable.css("height", "0%");
		else
			periodicTable.css("height", "100%");
	};
	helpButton.off("click", helpListener);
	helpButton.on("click", helpListener);
	
	typingText[0] = new TypingText(task, 50, true, function(){
		balls.fadeIn(500);
		textContainer.fadeIn(500);
		helpButton.fadeIn(500);
	});
	
	var successCondition = function(vegetable, basket)
	{
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket)
	{
		basket.html(vegetable.html());
		basket.css("color", vegetable.css("color"));
		basket.css("font-size", vegetable.css("font-size"));
		basket.css("font-style", vegetable.css("font-style"));
		vegetable.remove();
		oxygenAmount += 5;
		drawPointer();
	};
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
		keepThinking.fadeIn(0);
		timeout[3] = setTimeout(function(){
			keepThinking.fadeOut(0);
		}, 2000);
	};
	
	var finishCondition = function(vegetable, basket)
	{
		return $(prefix + ".ball").length <= 2;
	};
	
	var finishFunction = function(vegetable, basket)
	{
		drawPointer();
		audio[1].play();
		timeout[2] = setTimeout(function(){
			sendCompletedStatement(12);
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
		}, 5000);
	};
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	oxygenAmount -= 10;
	fadeNavsOut();
	fadeLauncherIn();
	keepThinking.fadeOut(0);
	textContainer.fadeOut(0);
	task.fadeOut(0);
	balls.fadeOut(0);
	helpButton.fadeOut(0);
	periodicTable.css("height", "0%");
		
	startButtonListener = function(){
		timeout[1] = setTimeout(function(){
			periodicTable.addClass("transition-2s");
			audio[0].play();
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(12);
	}, 2000);
}

launch["frame-113"] = function()
{
		theFrame = $("#frame-113"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		text = $(prefix + ".text"),
		task = $(prefix + ".task"),
		table = $(prefix + ".table"),
		inputs = $(prefix + "input"),
		checkButton = $(prefix + ".check-button"),
		helpButton = $(prefix + ".help-button"),
		periodicTable = $(prefix + ".periodic-table"),
		kalii = $(prefix + ".kalii"),
		natrii = $(prefix + ".natrii");
	
	audio[0] = new Audio("audio/sci-fi.mp3");
	audio[1] = new Audio("audio/s24-1.mp3");
	audio[2] = new Audio("audio/s27-1.mp3");
	audio[3] = new Audio("audio/s24_0_aliens_talking.mp3");
	audio[4] = new Audio("audio/s25-1.mp3");
		
	audio[0].addEventListener("ended", function(){
		this.play();
	});
	
	audio[1].addEventListener("ended", function(){
		audio[3].pause();
		audio[4].play();
		task.fadeIn(500);
		typingText[0].write();
	});
	
	audio[4].addEventListener("ended", function(){
		task.fadeIn(500);
		typingText[0].write();
	});
	
	audio[2].addEventListener("ended", function(){
		sendCompletedStatement(13);
		theFrame.attr("data-done", "true");
		fadeNavsInAuto();
	});
		
	typingText[0] = new TypingText(task, 50, true, function(){
		table.fadeIn(500);
		helpButton.fadeIn(0);
	});
	
	inputs.val("");
	fadeNavsOut();
	fadeLauncherIn();
	oxygenAmount -= 10;
	drawPointer();
	checkButton.fadeOut(0);
	text.fadeOut(0);
	table.fadeOut(0);
	task.fadeOut(0);
	periodicTable.css("height", "0%");
	helpButton.fadeOut(0);
	kalii.fadeOut(0);
	natrii.fadeOut(0);
	
	var helpListener = function(){
		if(parseInt(periodicTable.css("height")))
			periodicTable.css("height", "0%");
		else
			periodicTable.css("height", "100%");
	};
	helpButton.off("click", helpListener);
	helpButton.on("click", helpListener);
	
	var inputListener = function(){
		var emptyNum = 14;
		for(var i = 0; i < inputs.length; i++)
			if($(inputs[i]).val())
				emptyNum --;
			
		if(!emptyNum)
			checkButton.fadeIn(500);
	};
	inputs.on("keyup", inputListener); //jquery 
	
	var checkButtonListener = function(){
		for(var i = 0; i < inputs.length; i++)
		{
			if($(inputs[i]).val() === $(inputs[i]).attr("data-correct"))
			{
				oxygenAmount += 5;
				$(inputs[i]).css("color", "green");
			}
			else
			{
				$(inputs[i]).css("color", "red");
			}
			drawPointer();
		}
		
		timeout[2] = setTimeout(function(){
			checkButton.fadeOut(0);
			for(var i = 0; i < inputs.length; i++)
			{
				if($(inputs[i]).val() === $(inputs[i]).attr("data-correct"))
				{
					$(inputs[i]).css("color", "");
					$(inputs[i]).css("font-weight", "bold");
					$(inputs[i]).css("font-size", "1.3vmax");
				}
				else
				{
					$(inputs[i]).css("color", "");
					$(inputs[i]).css("color", "normal");
					$(inputs[i]).val($(inputs[i]).attr("data-correct"));
					$(inputs[i]).css("font-size", "1.1vmax");
				}
			}
		}, 3000);
		
		timeout[3] = setTimeout(function(){
			table.fadeOut(0);
			task.fadeOut(0);
			helpButton.fadeOut(0);
			checkButton.fadeOut(0);
			audio[2].play();
			timeout[4] = setTimeout(function(){
				kalii.fadeIn(500);
			}, 8000);
			timeout[5] = setTimeout(function(){
				kalii.fadeOut(500);
				natrii.fadeIn(500);
			}, 12000);
		}, 8000);
		
		drawPointer();
	}
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	startButtonListener = function(){
		timeout[1] = setTimeout(function(){
			periodicTable.addClass("transition-2s");
			text.fadeIn(1000);
			audio[0].volume = 0.3;
			audio[3].volume = 0.2;
			audio[1].play();
			audio[3].play();
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(13);
	}, 2000);
}

launch["frame-114"] = function()
{
		theFrame = $("#frame-114"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		astronautApproach = $(prefix + ".astronaut-approach"),
		astronautWave = $(prefix + ".astronaut-wave"),
		shipLeaving = $(prefix + ".ship-leaving");
	
	audio[0] = new Audio("audio/spacecraft-sound.mp3");
	audio[1] = new Audio("audio/s28-1.mp3");
	
	sprite[0] = new Motio(shipLeaving[0], {
		"fps": "2",
		"frames": "7"
	});
	
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			timeout[3] = setTimeout(function(){
				sendCompletedStatement(14);
				theFrame.attr("data-done", "true");
				fadeNavsInAuto();
				audio[0].pause();
			}, 2000);
		}
	});
	
	sprite[1] = new Motio(astronautApproach[0], {
		"fps": "3", 
		"frames": "6"
	});
	sprite[1].on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			audio[0].volume = 0.4;
			audio[0].play();
			astronautApproach.fadeOut(0);
			astronautWave.fadeIn(0);
			sprite[2].play();
			timeout[2] = setTimeout(function(){
				sprite[2].pause();
				astronautWave.fadeOut(0);
				theFrame.css("background-image", "url(pics/s27-bg-2.png)");
				shipLeaving.fadeIn(0);
				sprite[0].play();
			}, 3000);
		}	
	});
	
	sprite[2] = new Motio(astronautWave[0], {
		"fps": "3", 
		"frames": "3"
	});
	
	astronautWave.fadeOut(0);
	shipLeaving.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
		timeout[1] = setTimeout(function(){
			audio[1].play();
			sprite[1].play();
		}, 2000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(14);
	}, 2000);
}

launch["frame-201"] = function()
{
		theFrame = $("#frame-201"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		taskBG = $(prefix + ".task-bg"),
		balls = $(prefix + ".ball"),
		keepThinking = $(prefix + ".keep-thinking"),
		task = $(prefix + ".title");
		
		typingText[0] = new TypingText(task, 50, true, function(){
		balls.fadeIn(500);
		taskBG.fadeIn(500);
	});
	
	var successCondition = function(vegetable, basket)
	{
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket)
	{
		basket.html(vegetable.html());
		vegetable.remove();
		oxygenAmount += 10;
		drawPointer();
	};
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
		keepThinking.fadeIn(0);
		timeout[1] = setTimeout(function(){
			keepThinking.fadeOut(0);
		}, 3000);
	};
	
	var finishCondition = function(vegetable, basket)
	{
		return $(prefix + ".ball").length == 0;
	};
	
	var finishFunction = function(vegetable, basket)
	{
		theFrame.attr("data-done", "true");
		fadeNavsInAuto();
		sendCompletedStatement(201);
	};
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	keepThinking.fadeOut(0);
	balls.fadeOut(0);
	taskBG.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			taskBG.fadeIn(500);
			typingText[0].write();
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(201);
	}, 2000);
}

launch["frame-202"] = function()
{
		theFrame = $("#frame-202"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		taskBG = $(prefix + ".task-bg"),
		balls = $(prefix + ".ball"),
		keepThinking = $(prefix + ".keep-thinking"),
		task = $(prefix + ".title"),
		table = $(prefix + ".table");
		
		typingText[0] = new TypingText(task, 50, true, function(){
		balls.fadeIn(500);
		taskBG.fadeIn(500);
		table.fadeIn(500);
	});
	
	var successCondition = function(vegetable, basket)
	{
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket)
	{
		basket.html(vegetable.html());
		vegetable.remove();
		oxygenAmount += 10;
		drawPointer();
	};
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
		keepThinking.fadeIn(0);
		timeout[1] = setTimeout(function(){
			keepThinking.fadeOut(0);
		}, 3000);
	};
	
	var finishCondition = function(vegetable, basket)
	{
		return $(prefix + ".ball").length == 0;
	};
	
	var finishFunction = function(vegetable, basket)
	{
		theFrame.attr("data-done", "true");
		fadeNavsInAuto();
		sendCompletedStatement(202);
	};
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	keepThinking.fadeOut(0);
	balls.fadeOut(0);
	taskBG.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	table.fadeOut(0);
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			taskBG.fadeIn(500);
			typingText[0].write();
		});
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(202);
	}, 2000);
}

launch["frame-203"] = function()
{
	    theFrame = $("#frame-203"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		downloadButton = $(prefix + ".download"),
		textarea = $(prefix + "textarea"),
		title = $(prefix + ".title");
		
		typingText[0] = new TypingText(title, 100, true, function(){
		textarea.fadeIn(500);
	});
	
	var downloadButtonListener = function(){
		var blob = new Blob([textarea.val()], {type: "text/plain;charset=utf-8"});
		saveAs(blob, "My essay.txt");
		theFrame.attr("data-done", "true");
		sendCompletedStatement(203);
		fadeNavsInAuto();
	};
	downloadButton.off("click", downloadButtonListener);
	downloadButton.on("click", downloadButtonListener);
	
	textarea.val("");
	title.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			title.fadeIn(500);
			typingText[0].write();
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(203);
	}, 2000);
}

launch["frame-301"] = function()
{
	theFrame = $("#frame-301"),
	theClone = theFrame.clone();
}

launch["frame-302"] = function()
{
	theFrame = $("#frame-302"),
	theClone = theFrame.clone();
}

launch["frame-303"] = function()
{
	theFrame = $("#frame-303"),
	theClone = theFrame.clone();
}

launch["frame-304"] = function()
{
	theFrame = $("#frame-304"),
	theClone = theFrame.clone();
}

launch["frame-305"] = function()
{
	theFrame = $("#frame-305"),
	theClone = theFrame.clone();
}

launch["frame-306"] = function()
{
	theFrame = $("#frame-306"),
	theClone = theFrame.clone();
}

launch["frame-401"] = function()
{
	
}

var hideEverythingBut = function(elem)
{
	if(theFrame)
		theFrame.html(theClone.html());
	
	var frames = $(".frame");
	
	$(".o2").fadeIn(0);
	$(".o2").css({
		"width": "",
		"height": "",
		"left": "",
		"top": ""
	});
	
	frames.fadeOut(0);
	elem.fadeIn(0);
	elemId = elem.attr("id");
	regBox = $(".reg-box");
	fadeTimerOut();
	
	if(!globalName)
		regBox.fadeOut(0);
	
	if(elemId === "frame-000")
	{
		fadeNavsOut();
		fadeTimerOut();
	}
	
	for (var i = 0; i < audio.length; i++)
		audio[i].pause();	
	
	for (var i = 0; i < audioPiece.length; i++)
		audioPiece[i].pause();	

	for (var i = 0; i < timeout.length; i++)
	{
		clearTimeout(timeout[i]);
	}
	
	stopTimer();
	
	for (var i = 0; i < sprite.length; i++)
		sprite[i].pause();
	
	for (var i = 0; i < typingText.length; i++)
		typingText[i].stopWriting();
	
	if(elem.attr("id") === "frame-000")
		regBox.fadeIn(0);
	
	if(elem.hasClass("fact"))
	{
		$(".o2").fadeOut(0);
	}
	
	if(elem.attr("id") === "frame-000")
	{
		$(".o2").fadeOut(0);
		fadeNavsOut();
	}
	
	launch[elemId]();
	initMenuButtons();
}

var initMenuButtons = function(){
	var links = $(".link");
	links.click(function(){
		var elem = $("#"+$(this).attr("data-link"));
		hideEverythingBut(elem);
	});
};

var main = function()
{
	var video = $(".intro-video"),
		pic = $(".intro-pic");
	
	oxygenPointer = $(".o2-pointer");
	setLRSData();
	hideEverythingBut($("#frame-000"));
	setMenuStuff();
	video.attr("width", video.parent().css("width"));
	video.attr("height", video.parent().css("height"));
	video[0].play();
	
	timeout[0] = setTimeout(function(){
		video.hide();
	}, 10000);
	timeout[1] = setTimeout(function(){
		pic.hide(); 
	}, 15000);
};

$(document).ready(main);